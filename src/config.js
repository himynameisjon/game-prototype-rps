/* @flow */
export default {
  gameWidth: 760,
  gameHeight: 400,
  game: {
    maxFramesPerTurn: 3,
    maxTurnsPerRound: 30
  },
  actor: {
    maxHealth: 10,
    minHealth: 1,
    maxStun: 10,
    minStun: 0
  }
}
