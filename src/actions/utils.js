/* @flow */
import { BaseAction } from './DefaultActions'
import ActionQueue from './../actions/ActionQueue'

import Actor from './../entities/Actor'

export function resolveActionEffects (action: BaseAction, targetAction: BaseAction, source: Actor, target: Actor, targetQueue: ActionQueue, index: number) {
  if (action.hasPriority(targetAction)) {
    action.onSuccess(source, target, targetQueue, index)
  } else {
    action.onFail(source, target, targetQueue, index)
  }
}
