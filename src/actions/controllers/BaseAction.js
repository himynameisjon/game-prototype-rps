/* @flow */
declare var __DEV__: string;

import Phaser from 'phaser-ce'

import type { ActionInterface } from './../ActionInterface'
import type { ActionType } from './../ActionType'
import ActionQueue from './../ActionQueue'
import { StunActionType } from './../DefaultActionTypes'

import type { GameSprites } from './../../entities/GameSprites'
import Actor from './../../entities/Actor'

export default class BaseAction implements ActionInterface {
  action: ActionType;

  constructor (actionType: ActionType) {
    this.action = actionType
  }

  onAction (key: Phaser.Key, source: ActionQueue, target: ActionQueue, sprites: GameSprites) {
    if (!this.validate(source, target) || !source.add(this)) {
      // do error sprite
      return
    }

    if (__DEV__) {
      // console.log('BaseAction->onAction: ', source)
    }
  }

  getId () {
    return this.action.id
  }

  getKind () {
    return this.action.kind
  }

  getFrames () {
    return this.action.frameData.startup + this.action.frameData.active
  }

  getStartup () {
    return this.action.frameData.startup
  }

  getActive () {
    return this.action.frameData.active
  }

  getDamage () {
    return this.action.damage
  }

  getStun () {
    return this.action.stun
  }

  hasPriority (target: BaseAction) {
    return this.action.priority.indexOf(target.getKind()) !== -1
  }

  onSuccess (source: Actor, target: Actor, targetQueue: ActionQueue, index: number) {
    target.health -= this.getDamage()
    const stunIndex = index + 1
    targetQueue.insertAction(new BaseAction(StunActionType), stunIndex, stunIndex + this.getStun())
    console.log(`Success: ${source.id} [${this.getId()}] -> ${target.id} [${JSON.stringify(targetQueue.getAll().slice(-1).pop().getId())}]`)
  }

  onFail (source: Actor, target: Actor, targetQueue: ActionQueue, index: number) {
    console.log(`Fail: ${source.id} [${this.getId()}] -> ${target.id} [${JSON.stringify(targetQueue.getAll().slice(-1).pop().getId())}]`)
  }

  validate (source: ActionQueue, target: ActionQueue) {
    return true
  }
}
