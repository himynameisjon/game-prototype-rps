/* @flow */
declare var __DEV__: string;

import { Key } from 'phaser-ce'

import type { ActionType } from './../ActionType'
import BaseAction from './BaseAction'
import ActionQueue from './../ActionQueue'
import type { GameSprites } from './../../entities/GameSprites'

export default class extends BaseAction {
  action: ActionType;

  onAction (key: Key, source: ActionQueue, target: ActionQueue, sprites: GameSprites) {
    super.onAction(key, source, target, sprites)
    sprites.buttons.heavyAttack.toggle()
  }
}
