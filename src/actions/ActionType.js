/* @flow */
export type ActionType = {
  id: string,
  kind: string,
  priority: string[],
  damage: number,
  stun: number,
  frameData: {
    startup: number,
    active: number
  }
}
