/* @flow */
import { Key } from 'phaser-ce'

import ActionQueue from './ActionQueue'

import type { GameSprites } from './../entities/GameSprites'
import Actor from './../entities/Actor'

export interface ActionInterface {
  onAction (key: Key, source: ActionQueue, target: ActionQueue, sprites: GameSprites): void;
  onSuccess (source: Actor, target: Actor, targetQueue: ActionQueue, index: number): void;
  onFail (source: Actor, target: Actor, targetQueue: ActionQueue, index: number): void;
  validate (source: ActionQueue, target: ActionQueue): boolean;
}

export class NullAction implements ActionInterface {
  onAction (source: ActionQueue, target: ActionQueue, sprites: GameSprites) {}
  onSuccess (source: Actor, target: Actor, targetQueue: ActionQueue, index: number) {}
  onFail (source: Actor, target: Actor, targetQueue: ActionQueue, index: number) {}
  validate (source: ActionQueue, target: ActionQueue) { return false }
}
