import BaseAction from './controllers/BaseAction'
import { StartUpActionType } from './DefaultActionTypes'
import config from './../config'

export default class ActionQueue {
  actions: Array;
  frames: number;
  maxFrames: number;

  constructor () {
    this.actions = []
    this.frames = 0
    this.maxFrames = config.game.maxFramesPerTurn
  }

  add (action: BaseAction) {
    const futureFrames = this.frames + action.getFrames()

    if (futureFrames > this.maxFrames) {
      return false
    }

    this.frames = futureFrames
    this.fill(new BaseAction(StartUpActionType), action.getStartup())
    this.fill(action, action.getActive())

    return true
  }

  getAll () {
    return this.actions.concat()
  }

  fill (action: BaseAction, count: number = 1) {
    let i = 0
    while (i < count) {
      this.actions.push(action)
      i++
    }
  }

  fillAll (action: BaseAction) {
    while (this.add(action)) {
      // console.log('[ActionQueue] Added dummy action:', action)
    }
  }

  insertAction (action, start, count) {
    this.actions.fill(action, start, count)
  }

  removeAll () {
    this.actions.length = 0
    this.frames = 0
  }
}
