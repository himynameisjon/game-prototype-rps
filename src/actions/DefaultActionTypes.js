/* @flow */
import type { ActionType } from './ActionType'

export const NullEffect = 'NullEffect'
export const AttackEffect = 'AttackEffect'
export const BlockEffect = 'BlockEffect'
export const ThrowEffect = 'ThrowEffect'

export const NullActionType: ActionType = {
  id: 'NullActionType',
  kind: NullEffect,
  priority: [
    NullEffect
  ],
  damage: 0,
  stun: 0,
  frameData: {
    startup: 0,
    active: 0
  }
}

export const StartUpActionType: ActionType = {
  id: 'StartUpActionType',
  kind: NullEffect,
  priority: [
    NullEffect
  ],
  damage: 0,
  stun: 0,
  frameData: {
    startup: 0,
    active: 0
  }
}

export const StunActionType: ActionType = {
  id: 'StunActionType',
  kind: NullEffect,
  priority: [],
  damage: 0,
  stun: 0,
  frameData: {
    startup: 0,
    active: 0
  }
}

export const AttackActionType: ActionType = {
  id: 'JabActionType',
  kind: AttackEffect,
  priority: [
    AttackEffect,
    ThrowEffect,
    NullEffect
  ],
  damage: 1,
  stun: 1,
  frameData: {
    startup: 0,
    active: 1
  }
}

export const HeavyAttackActionType: ActionType = {
  id: 'HeavyAttackActionType',
  kind: AttackEffect,
  priority: [
    AttackEffect,
    ThrowEffect,
    NullEffect
  ],
  damage: 2,
  stun: 2,
  frameData: {
    startup: 1,
    active: 1
  }
}

export const BlockActionType: ActionType = {
  id: 'BlockActionType',
  kind: BlockEffect,
  priority: [
    BlockEffect,
    AttackEffect,
    NullEffect
  ],
  damage: 0,
  stun: 1,
  frameData: {
    startup: 0,
    active: 1
  }
}

export const ThrowActionType: ActionType = {
  id: 'ThrowActionType',
  kind: ThrowEffect,
  priority: [
    BlockEffect,
    NullEffect
  ],
  damage: 1,
  stun: 1,
  frameData: {
    startup: 0,
    active: 1
  }
}
