/* @flow */
import { Sprite } from 'phaser-ce'

export const centerGameObjects = (objects: Sprite[]) => {
  objects.forEach(function (object) {
    object.anchor.setTo(0.5)
  })
}
