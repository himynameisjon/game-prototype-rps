/* @flow */
import Phaser from 'phaser-ce'

export default class extends Phaser.State {
  init () {}

  preload () {
  }

  create () {
    let text = this.add.text(this.world.centerX, this.world.centerY, 'Game Over', { font: '32px Inconsolata', fill: '#ffffff', align: 'center' })
    text.anchor.setTo(0.5, 0.5)
  }

}
