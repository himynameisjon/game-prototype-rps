/* @flow */
import Phaser from 'phaser-ce'
import { centerGameObjects } from '../utils'
import localAssets from './../assets/ui.json'

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)

    //
    // load your assets
    //
    this.game.load.atlasJSONArray('ui', './../assets/ui.png', null, localAssets)
  }

  create () {
    this.state.start('Game')
  }

}
