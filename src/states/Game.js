/* @flow */
declare var __DEV__: string;

import Phaser, { Timer, Text } from 'phaser-ce'

import { AttackActionType, HeavyAttackActionType, ThrowActionType, BlockActionType } from '../actions/DefaultActionTypes'
import { BaseAction, BlockAction, ThrowAction, AttackAction, HeavyAttackAction } from './../actions/DefaultActions'
import ActionQueue from './../actions/ActionQueue'
import { resolveActionEffects } from './../actions/utils'

import type { GameSprites } from './../entities/GameSprites'
import Button from '../entities/Button'
import Actor from './../entities/Actor'

import config from './../config'

type GameAssets = {
  buttons: {
    block: Button;
    throw: Button;
    attack: Button;
    heavyAttack: Button;
  },
  actors: {
    player: Actor;
    opponent: Actor;
  },
  timer: Text;
  health: {
    player: Text;
    opponent: Text;
  }
}

export default class extends Phaser.State {
  assets: GameAssets;

  init () {
    this.queue = this.createQueue()
    this.history = this.createQueue()
    this.remainingTurns = config.game.maxTurnsPerRound
  }

  createQueue () {
    return {
      player: new ActionQueue(),
      opponent: new ActionQueue()
    }
  }

  preload () {
    this.assets = {
      buttons: {
        block: new Button({ game: this, x: 50, y: 300, sheet: 'ui', upFrame: 'buttons/grey/up', downFrame: 'buttons/grey/down' }),
        throw: new Button({ game: this, x: 100, y: 300, sheet: 'ui', upFrame: 'buttons/blue/up', downFrame: 'buttons/blue/down' }),
        attack: new Button({ game: this, x: 620, y: 300, sheet: 'ui', upFrame: 'buttons/green/up', downFrame: 'buttons/green/down' }),
        heavyAttack: new Button({ game: this, x: 670, y: 300, sheet: 'ui', upFrame: 'buttons/red/up', downFrame: 'buttons/red/down' })
      },
      actors: {
        player: new Actor({ id: 'PLAYER', game: this, x: 50, y: 175, sheet: 'ui', frame: 'buttons/blue/up' }),
        opponent: new Actor({ id: 'OPPONENT', game: this, x: 670, y: 175, sheet: 'ui', frame: 'buttons/red/up' })
      },
      timer: this.add.text(this.world.centerX, 25, this.remainingTurns.toString(), { font: '48px Inconsolata', fill: '#ffffff', align: 'center' }),
      health: {
        player: this.add.text(50, 25, config.actor.maxHealth.toString(), { font: '48px Inconsolata', fill: '#ffffff', align: 'center' }),
        opponent: this.add.text(670, 25, config.actor.maxHealth.toString(), { font: '48px Inconsolata', fill: '#ffffff', align: 'center' })
      }
    }

    this.assets.timer.anchor.setTo(0.5, 0.5)

    this.game.add.existing(this.assets.actors.player)
    this.game.add.existing(this.assets.actors.opponent)
    this.game.add.existing(this.assets.buttons.block)
    this.game.add.existing(this.assets.buttons.throw)
    this.game.add.existing(this.assets.buttons.attack)
    this.game.add.existing(this.assets.buttons.heavyAttack)

    this.timer = this.game.time.events.loop(Timer.SECOND * 3, () => { this.processTurnActions(this.queue.player, this.queue.opponent, this.assets) })
  }

  processTurnActions (player: ActionQueue, opponent: ActionQueue, assets: GameAssets) {
    player.fillAll(new BlockAction(BlockActionType))
    opponent.fillAll(new BlockAction(BlockActionType))

    for (let index = 0; index < config.game.maxFramesPerTurn; index++) {
      resolveActionEffects(player.actions[index], opponent.actions[index], assets.actors.player, assets.actors.opponent, opponent, index)
      resolveActionEffects(opponent.actions[index], player.actions[index], assets.actors.opponent, assets.actors.player, player, index)
    }

    this.history.player = player.getAll()
    this.history.opponent = opponent.getAll()

    player.removeAll()
    opponent.removeAll()

    assets.timer.setText(this.remainingTurns.toString())
    assets.health.player.setText(assets.actors.player.health.toString())
    assets.health.opponent.setText(assets.actors.opponent.health.toString())

    if (this.remainingTurns-- < 0 || assets.actors.player.health < config.actor.minHealth || assets.actors.opponent.health < config.actor.minHealth) {
      this.endGame()
    }
  }

  endGame () {
    this.state.start('GameOver')
  }

  create () {
    this.bindKeyboardInputs()
  }

  update () {
  }

  render () {
    if (__DEV__) {
      // debug
    }
  }

  bindKeyboardInputs () {
    const bind = this.bindKeyboardInput.bind(this, this.queue.player, this.queue.opponent, this.assets)

    bind(Phaser.Keyboard.A, new BlockAction(BlockActionType))
    bind(Phaser.Keyboard.S, new ThrowAction(ThrowActionType))
    bind(Phaser.Keyboard.D, new AttackAction(AttackActionType))
    bind(Phaser.Keyboard.SPACEBAR, new HeavyAttackAction(HeavyAttackActionType))
  }

  // input
  bindKeyboardInput (source: ActionQueue, target: ActionQueue, assets: GameSprites, keyCode: Phaser.Key, action: BaseAction) {
    const inputKey = this.game.input.keyboard.addKey(keyCode)
    inputKey.onUp.add(action.onAction.bind(action), this, 0, source, target, assets)

    return inputKey
  }
}
