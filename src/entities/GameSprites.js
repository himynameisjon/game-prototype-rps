/* @flow */
import { Sprite } from 'phaser-ce'

export type GameSprites = {
  buttons: {
    block: Sprite,
    throw: Sprite,
    attack: Sprite,
    heavyAttack: Sprite
  }
}
