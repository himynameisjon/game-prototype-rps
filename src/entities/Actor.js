/* @flow */
import { Sprite } from 'phaser-ce'

import type { ActorConfig } from './SpriteConfig'
import config from './../config'

export default class extends Sprite {
  id: string = 'Actor';
  maxHealth: number = config.actor.maxHealth;
  health: number = config.actor.maxHealth;
  maxStun: number = config.actor.maxStun;
  stun: number = config.actor.minStun;

  constructor ({ game, x, y, sheet, frame, id }: ActorConfig) {
    super(game, x, y, sheet, frame)

    this.id = id
  }
}
