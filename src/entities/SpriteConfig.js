/* @flow */
import { Game } from 'phaser-ce'

export type ActorConfig = {
  game: Game,
  x: number,
  y: number,
  sheet: string,
  frame: string,
  id: string
}

export type ButtonConfig = {
  game: Game,
  x: number,
  y: number,
  sheet: string,
  upFrame: string,
  downFrame: string
}
