/* @flow */
import { Sprite } from 'phaser-ce'
import type { ButtonConfig } from './SpriteConfig'

export default class extends Sprite {
  constructor ({ game, x, y, sheet, upFrame, downFrame }: ButtonConfig) {
    super(game, x, y, sheet, upFrame)

    this.toggleDistanceY = 4
    this.toggleDuration = 120
    this.upFrame = upFrame
    this.downFrame = downFrame
  }

  toggle () {
    this.toggleDown()
    this.game.time.events.add(this.toggleDuration, this.toggleUp, this)
  }

  toggleDown () {
    if (this.frameName === this.downFrame) return

    this.frameName = this.downFrame
    this.y += this.toggleDistanceY
  }

  toggleUp () {
    if (this.frameName === this.upFrame) return

    this.frameName = this.upFrame
    this.y -= this.toggleDistanceY
  }
}
